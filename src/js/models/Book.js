var mODELcLASS = require('mODELcLASS');

module.exports = new mODELcLASS({
  name: 'Book',
  properties: {
    isbn: {
      label: 'ISBN',
      range: 'NonEmptyString',
      isStandardId: true,
      pattern: /\b\d{9}(\d|X)\b/,
      patternMessage: 'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'
    },

    title: {
      label: 'Title',
      range: 'NonEmptyString',
      min: 2,
      max: 50
    },

    year: {
      label: 'Year',
      range: 'Integer',
      min: 1459,
      max: (new Date()).getFullYear()
    },

    publisher: {
      className: 'Publisher',
      label: 'Publisher',
      range: 'SingleValueRef',
      inverseRef: {
        name: 'publishedBooks',
        range: 'MultiValueRef'
      },
      optional: true
    }
  }
});