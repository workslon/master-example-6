var eNUMERATION = require('eNUMERATION');

module.exports = new eNUMERATION('Statuses', [
  'idle',
  'pending',
  'success',
  'error'
]);