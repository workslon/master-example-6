var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';

var notifications = {
  status: StatusConstants.IDLE,
  errors: {}
};

var NotificationStore = Object.assign({}, EventEmitter.prototype, {
  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Create book Pending
    case AppConstants.REQUEST_BOOK_SAVE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Create book Success
    case AppConstants.BOOK_SAVE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Create book Error
    case AppConstants.BOOK_SAVE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Update book Pending
    case AppConstants.REQUEST_BOOK_UPDATE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // --- Update book Success
    case AppConstants.BOOK_UPDATE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Update book Error
    case AppConstants.BOOK_UPDATE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Destroy book Pending
    case AppConstants.REQUEST_BOOK_DESTROY:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Destroy book Success
    case AppConstants.BOOK_DESTROY_SUCCESS:
      notifications.status = StatusConstants.IDLE;
      NotificationStore.emitChange();
      break;

    // --- Non-unique isbn
    case AppConstants.NON_UNIQUE_ISBN:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = {
        isbn: 'The book with such ISBN already exist!'
      };
      NotificationStore.emitChange();
      break;

    // --- Client Validation error
    case AppConstants.BOOK_VALIDATION_ERROR:
      Object.keys(action.errors).map(function (key) {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = StatusConstants.ERROR;
      NotificationStore.emitChange();
      break;

    // -- Create publisher Pending
    case AppConstants.REQUEST_PUBLISHER_SAVE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Create publisher Success
    case AppConstants.PUBLISHER_SAVE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Create publisher Error
    case AppConstants.PUBLISHER_SAVE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Update publisher Pending
    case AppConstants.REQUEST_PUBLISHER_UPDATE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // --- Update publisher Success
    case AppConstants.PUBLISHER_UPDATE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Update publisher Error
    case AppConstants.PUBLISHER_UPDATE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Destroy publisher Pending
    case AppConstants.REQUEST_PUBLISHER_DESTROY:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Destroy publisher Success
    case AppConstants.PUBLISHER_DESTROY_SUCCESS:
      notifications.status = StatusConstants.IDLE;
      NotificationStore.emitChange();
      break;

    // --- Client Validation error
    case AppConstants.PUBLISHER_VALIDATION_ERROR:
      Object.keys(action.errors).map(function (key) {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = StatusConstants.ERROR;
      NotificationStore.emitChange();
      break;

    // --- Non-unique email
    case AppConstants.NON_UNIQUE_EMAIL:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = {
        email: 'The publisher with such email already exist!'
      };
      NotificationStore.emitChange();
      break;

    // --- Clear all notifications (eather errors or success)
    case AppConstants.CLEAR_NOTIFICATIONS:
      notifications = {
        status: StatusConstants.IDLE,
        errors: {}
      };
      NotificationStore.emitChange();
      break;
  }
});

module.exports = NotificationStore;