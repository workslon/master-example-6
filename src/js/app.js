var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');

var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;

var App = require('./components/App.react');
var BookList = require('./components/BookList.react');
var CreateBook = require('./components/CreateBook.react');
var UpdateBook = require('./components/UpdateBook.react');
var PublisherList = require('./components/PublisherList.react');
var CreatePublisher = require('./components/CreatePublisher.react');
var UpdatePublisher = require('./components/UpdatePublisher.react');

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={BookList} />
      <Route path="books/create" component={CreateBook} />
      <Route path="books/update/:id" component={UpdateBook} />
      <Route path="publishers/list" component={PublisherList} />
      <Route path="publishers/create" component={CreatePublisher} />
      <Route path="publishers/update/:id" component={UpdatePublisher} />
    </Route>
  </Router>
), document.getElementById('app'));