var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: AppConstants.BOOK_VALIDATION_ERROR,
      errors: errors
    });
  },

  getBooks: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOKS,
      success: AppConstants.REQUEST_BOOKS_SUCCESS,
      failure: AppConstants.REQUEST_BOOKS_ERROR
    });
  },

  createBook: function (modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.isbn)
      .then(function(records) {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: AppConstants.NON_UNIQUE_ISBN
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: AppConstants.REQUEST_BOOK_SAVE,
              success: AppConstants.BOOK_SAVE_SUCCESS,
              failure: AppConstants.BOOK_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_BOOK_SAVE
    });
  },

  updateBook: function (modelClass, book, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, book, newData);
    newData.objectId = book.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOK_UPDATE,
      success: AppConstants.BOOK_UPDATE_SUCCESS,
      failure: AppConstants.BOOK_UPDATE_ERROR
    }, newData);
  },

  deleteBook: function (modelClass, book) {
    var promise;

    book.runCloudCode = true;
    promise = adapter.destroy(modelClass, book);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOK_DESTROY,
      success: AppConstants.BOOK_DESTROY_SUCCESS,
      failure: AppConstants.BOOK_DESTROY_ERROR
    }, book);
  }
};