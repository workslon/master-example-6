var React = require('react');
var Link = require('react-router').Link;
var PublisherActions = require('../actions/PublisherActions');
var PublisherModel = require('../models/Publisher');

module.exports = React.createClass({
  displayName: 'Publisher',

  _deletePublisher: function () {
    PublisherActions.deletePublisher(PublisherModel, this.props.publisher);
  },

  render: function () {
    var publisher = this.props.publisher;
    var books = publisher.publishedBooks || [];
    var updatePath = '/publishers/update/' + publisher.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{publisher.name}</td>
        <td>{publisher.email}</td>
        <td>
          <ul>{books.map(function (book) {
            return <li key={book.objectId}>{book.isbn} - {book.title}</li>
          })}</ul>
        </td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deletePublisher}>Delete</a>
        </td>
      </tr>
    );
  }
});