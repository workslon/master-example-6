var React = require('react');
var ReactDOM = require('react-dom');
var BookActions = require('../actions/BookActions');
var BookStore = require('../stores/BookStore');
var PublisherStore = require('../stores/PublisherStore');
var StatusConstants = require('../constants/StatusConstants');
var BookModel = require('../models/Book');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdateBook',

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.title).focus();
  },

  componentWillMount: function() {
    this.book = BookStore.getBook(this.props.params.id);
  },

  _updateBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};
    var publisher = refs.publisher || {};

    BookActions.updateBook(BookModel, this.book, {
      title: title.value,
      year: parseInt(year.value),
      publisher: {
        objectId: publisher.value
      }
    });
  },

  _validate: function(e) {
    BookActions.validate(BookModel, e.target.id, e.target.value);
  },

  render: function () {
    var publishers = PublisherStore.getAllPublishers();
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Book</h3>
          {this.book ?
            <form>
              <div className="form-group">
                <label htmlFor="isbn">ISBN</label>
                <span>{this.book.isbn}</span>
              </div>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input defaultValue={this.book.title} onInput={this._validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
                {errors.title && <span className="text-danger">{errors.title}</span>}
              </div>
              <div className="form-group">
                <label htmlFor="year">Year</label>
                <input defaultValue={this.book.year} onInput={this._validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
                {errors.year && <span className="text-danger">{errors.year}</span>}
              </div>
              <div className="form-group">
                <label htmlFor="year">Publishers</label>
                <select defaultValue={this.book.publisher ? this.book.publisher.objectId : ''} className="form-control" ref="publisher">
                  <option value="">-No Publisher-</option>
                  {publishers.map(function (publisher) {
                    return <option value={publisher.objectId} key={publisher.objectId}>{publisher.name} - {publisher.email}</option>
                  })}
                </select>
              </div>
              <button type="submit" onClick={this._updateBook} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});